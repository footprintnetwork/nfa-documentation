This README describes the general NFA production process. For detailed information on individual MySQL scripts please see the readme file in the repository corresponding to the dataset of interest and [this document](https://www.dropbox.com/s/q5gbst0wh8a2j93/manual-nfa2017_update.docx?dl=0). It should be noted early on that the NFA production process is an iterative process that almost definitely requires many versions before successfully creating the desired final product due the great multitude of input tables, varying data sources and naming conventions, undisclosed or poorly documented changes to data collection by the data sources, independent data structures (software, applications, et al) amongst other possible complications. For a detailed schematic of this process see this [excel document](https://www.dropbox.com/s/xvl3dnat3pm2vao/NFA-SSRI.xls?dl=0).

### What is this repository for? ###
This repository is for housing general high-level, organizational documentation on the NFA production process. This document should be read before beginning the Asana NFA 2019 Production project, Baseline Production task.

* Background

	The production of the National Footprint Accounts is a fairly technical process. There are many steps involved in order to go from the many raw, disparate data sets to the nice and tidy timeseries data for all, approximately, 242 countries, 12 record types, 54 (and counting) years and seven footprint types.  
	
	The "schedule" or life-cycle of the NFA production is, as stated above, an iterative process that can be broken down into two phases; the baseline production and the ult production phases. Each of these phases are defined by NFA batches that are numbered sequentially. For example the baseline batches are numbered footprint_yy_baselineN, footprint_yy_baselineN+1, etc. and the same naming convention goes for the ult batches. The baseline phase graduates to the ult phase when a baseline batch has been produced and QA'ed to show that it reproduces the previous years' published NFA results (plus one new final year). This is to say, it is a batch that is the minimum viable product if no improvements (methodology or otherwise) are made to this year's results. 
	
* Version
	
	This is the first version of the NFA Documentation readme.md file. May it be the first of many!

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###


- You will need the following in order to begin to use any upload scripts;
	 1.	A MySQL Database
	 2.	A MySQL GIUI client (MySQL Workbench)
	 3.	A GIT client (current source code is in Bitbucket).
 	 4.	MySQL command client to run scripts.
	 5.	A text/source code editor like notepad ++ or sublime 
	 6.	A windows operating system in order to follow the instructions below to run tests
	 7.	Excel 2016
	 8.	Matlab
	 9.	R and R client 


* Deployment instructions
	
	- Navigating bitbucket to use the MySQL upload scripts.
	 
		First, you will need to create a local folder into which clone the repository where the upload script is saved.  For more information on how to do this see the following [link](https://bitbucket.org/tutorials/bucket-o-sand).  
		
	- Opening the upload script and preparing it to be run
		
		Once the script has been successfully pulled down from the repository, you must open it in order to prepare it to be run. 
		
		* The first thing one should do once the script is open is to read the comment section (header) at the beginning of the script. This section has a lot of useful information about important details the user should be aware of with respect to that specific script.
		
		* Next press Ctrl + F and type "Update" in the search field to find all areas of the script where there is an object that needs to be updated.
		
		* Lastly, make sure that the 'TEE:' command, which writes the outfile for this scripts processes, is properly written with the format
		
		````TEE: file/path/of/out/file.out```` and that it is written to a __logical location__ for the user to find once the script has finished running.
	
	- Running scripts through the command line interface
	
		In order that there be a log file that tracks errors and processing times for running the MySQL scripts, and to reduce the possibility of failure to update scripts as they are fixed, there is a protocol of running the scripts through the command log interface rather than through the MySQL GUI (Graphic User Interface, ie. the part of the software that you would normally interact with).	
		
		The process for doing that is as follows:
		
		* Click START, then RUN… in Windows. In the little dialogue box that pops up, type in “cmd” and hit OK.
		
		* A new window pops up which is the command prompt interface. As a default it has a black background with white type. There will be a flashing underline ready for typing.
		
		* The first thing to do is move the command prompt directory to the folder where the scripts you want to run are located.
		
			- Open the repo folder on your desktop in Windows Explorer, navigate to the folder that holds the script(s) you want to run and copy that URL.
			
			- In the command window, when the underline is flashing type “cd <space> “(cd is change directory) and paste in the address/URL of the folder with the scripts.  
			
			- Type “dir” to see all the files in that folder listed. This will help you reference and run them (dir is list directory).
	
		Next, we ‘open into MySQL‘.
		
		* Type 
	
			“MySQL  –h <database>  –u <username>  –p"
			
			* h is host. Database is “Orinda” for NFA, but you will want to test your scripts on your own computer first in which case you will write “localhost”.
			
			* u is user. On your local computer you will probably use root. When you are working on the production database you will need to use the appropriate username.
			
			* p is password. On your local computer you will probably use root or "<blank>". When you are working on the production database you will need to use the appropriate password. You will need to get this password from the IT manager if and when you have permission to write to that database.
			
		* When you are prompted, enter the password and hit ENTER. If you are working on your local computer the password should either be <blank> or root. If it is <blank> just hit enter. Now you should have a prompt that looks like
			
			'Mysql>'
			
To run a script from the current directory folder, select the name of the file from the list that appeared after you entered the “dir” command (Right click over the file name and choose ‘mark’ from the context menu. Click and hold left mouse button, scroll over the full file name, release the left mouse button and hit the ENTER key).	

Go to the command prompt line. Type “source “ and paste the file name (right click and choose ‘paste’).

NOTE: To end a running script before it’s done, or to go back up in the directory tree (eg. To go back out of MySQL, and/or back to your own U: drive etc.) hold CONTROL and hit C (for Cancel).


*  Dependencies - below is a table showing all the scripts necessary for running a single NFA batch. This table lists the order that the script needs to be run with respect to the other scripts. NB: for a script that has the same order as other scripts, it can be thought of as groups for which the order within the group does not matter but for which between different groups does matter. For example, the scripts FAOstat_upload.sql and ComtradeDownload.sln are both in the 1st order but can be run in any order in relation to each other. However, since they both come before the landarea_upload.sql script they both need to be run before. It should also be noted that this order is based on the output of these scripts and thus the order is a strict function of the dependency (input tables) for each script. For a complete list of these relations please see this excel document.
Lastly, there are two dependency tables (country_names... and country_matrix...) that must be added to the database before any scripts can be run. For each new NFA edition, the country_names... table is a copied version of the previous year, and renamed in the same schema as the previous year, with the naming convention country_names_20yy where yy is the last two digits of the year of the NFA edition. This new version then gets updated by the upload scripts through a MySQL country mapping statement the first time each script runs and finds a new country name. Please see this excel document for how this table was originally constructed. The second table, country_matrix... (VERIFY WITH DAVID, LAUREL) is also copied from the previous year, and renamed in the same schema, with the naming convention country_matrix_20yy where yy is the last two digits of the year of the NFA edition. 

	Order to be run |SCRIPT Name  | Dependencies
	:------------|:------------|:----------------
	1st | Faostat_upload.sql| country_data.country_names_20yy 
	1st | Faostat_upload.sql| country_data.country_matrix_20yy
	1st | ComtradeDownload.sln | none
	1st | Fishstat_upload.sql | country_data.country_names_20yy
	2nd | Landarea_upload.sql | country_data.country_names_20yy
	2nd | Landarea_upload.sql | faostat_yy_v1_ult.popstat_ult
	2nd | Landarea_upload.sql | faostat_yy_v1_ult_hketc.popstat_ult 
	3rd | comtrade_upload.sql | comtrade_yy_v1_raw.comtrade_csv
	3rd | comtrade_upload.sql | country_data.country_names_20yy
	3rd | comtrade_upload.sql | country_data.country_matrix_20yy 
	4th | comtrade_penult_cleaned.R | comtrade_yy_v1_sql.comtrade_penult
	5th | manual_data_cleaner.sql | comtrade_yy_v1_sql.comtade_penult_cleaned
	6th | test_for_comtrade_18 | comtrade_yy_v1_sql.comtrade_penult_cleaned_mdc
	6th | test_for_tradestat_18 | faostat_yy_v1_sql.tradestat
	6th | test_for_fishstat_18 | fishstat_yy_v1_sql.commodity_trpse
	7th | comtrade_livestock.sql | comtrade_yy_v1_ult.comtrade_ult_newoutlier_mdc
	7th | comtrade_livestock.sql | faostat_yy_v1_ult.tradestat_ult
	7th | comtrade_livestock.sql | faostat_yy_v1_sql.prodstat_livestock
	7th | comtrade_livestock.sql | faostat_yy_v1_sql.tradestat
	7th | comtrade_livestock.sql | faostat_yy_v1_ult.prodstat_livestock_ult
	7th | comtrade_livestock.sql | faostat_yy_v1_ult.resourcestat_livestock_ult
	7th | comtrade_livestock.sql | country_data.country_matrix_2017
	8th | Post_dataclean_china_aggsplit.sql | comtrade_yy_v1_ult.comtrade_newoutlier_mdc
	8th | Post_dataclean_china_aggsplit.sql | faostat_yy_v1_sql.tradestat
	8th | Post_dataclean_china_aggsplit.sql | fishstat_yy_v1.commodity_ult
	8th | Post_dataclean_china_aggsplit.sql | country_data.country_matrix_2017
	8th | Post_dataclean_china_aggsplit.sql | comtrade_17_v1_sql.commodity_key
	8th | Post_dataclean_china_aggsplit.sql | comtrade_17_v1_sql.unit_key
	8th | Post_dataclean_china_aggsplit.sql | fishstat_17_v1_ult_hketc.commodity_ult
	9th | CO2_Upload.sql | economic_data.worldbank_gdp_2015_trpse
	9th | CO2_Upload.sql |faostat_17_v1_ult.popstat_ult
	9th | CO2_Upload.sql |comtrade_17_v1_ult.comtrade_newoutlier_mdc
	9th | CO2_Upload.sql |country_data.country_matrix_2017
	9th | CO2_Upload.sql |country_data.country_names_2017
	9th | CO2_Upload.sql |comtrade_17_v1_ult_hketc.comtrade_ult_newoutlier_mdc
	10th| __RUN A BATCH__ |
	11TH| QA_diagnostic.sql | footprint_yy_baselinex.prev_batch_summary_results
	__AFTER ALL BATCHES FOR THE YEAR EDITION HAVE RUN__| Batch_QA.sql | 
	
----------------------


### Contribution guidelines ###

As stated in point 3 of the 'How do I get set up?' section above, a system for version control needs to be used in order to avoid copies of scripts getting branched into irreconcilable differences.
Commit relevant versions of scripts and add helpful comments to the window you will be prompted with before pushing your commit. 


####Writing tests####
		
After every time that you run a script, you should look through the associated .out file which was created. Open it as a text file and use the CTRL-F find function to check all listings of the word “ERROR” and the word “Warning”.	
	

####Code review####
	
Use these ERROR messages to fix the issues in the script that were encountered while it was running.
	
Make sure to commit your changes/corrections to server version.  
	
NOTE: Do not ‘commit’ the .out files. Right click on the .out file, edit the .gitignore file and add *.out to ignore all .out files in the project. Once you have set the folder to ignore .out files, go up to the folder level and commit that folder itself to save the change in the server.

And once you have completed running the script successfully, copy the associated .out file to the dot out log files of sql script runs sub-folder in the relevant Data Upload[Data Source] folder. This is so that it is available for future reference in case it may help identify the cause of problems that might appear later.	

####Other guidelines####

### Who do I talk to? ###

* Repo owner or admin

	Dr. David Lin
	
* Other community or team contact

	Mikel Evans